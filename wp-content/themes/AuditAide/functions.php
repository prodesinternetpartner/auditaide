<?php

function child_enqueue_scripts() {
	wp_register_style( 'childtheme_style', get_stylesheet_directory_uri() . '/style.css'  );
	wp_enqueue_style( 'childtheme_style' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts');


# Make sure no one can delete the prodes user
add_filter(
  'user_row_actions',
  function($actions, $user_object) {
    if ($user_object->user_login == 'prodes') {
      unset($actions['delete']);
  }
  return $actions;
},
1,2
);


/* Change the login style */
function prodes_login_style() {
 wp_enqueue_style( 'custom-login', 'https://www.prodes-cdn.com/wordpress-login.css' );
 wp_enqueue_script( 'custom-login', 'https://www.prodes-cdn.com/wordpress-login.js' );
}

add_action( 'login_enqueue_scripts', 'prodes_login_style' );

add_action( 'login_enqueue_scripts', 'prodes_login_style' );


/**
 * remove the register link from the wp-login.php script
 */
add_filter('option_users_can_register', function($value) {

    $script = basename(parse_url($_SERVER['SCRIPT_NAME'], PHP_URL_PATH));

    if($script == 'wp-login.php') {
        $value = false;
    }

    return $value;
});


/**
* Overwrite the target of the Prodes logo on the wp-admin login-page
*/
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );