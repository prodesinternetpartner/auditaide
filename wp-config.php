<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'audit_db');

/** MySQL database username */
define('DB_USER', 'audit_db');

/** MySQL database password */
define('DB_PASSWORD', 'Yg2op4rA3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZE-*ix68g,6S{g83_TxTnjAqTU:!Js+|>FdpH*ztK.*ALW-UHAuc`.v8FoaJ29Z0');
define('SECURE_AUTH_KEY',  '-:r[=+Z`zWy$Wd N>kl{>J&U2E8Dhe)4Pc V+RWdg~]HbA&0SH-!=;3|=lz 3gJr');
define('LOGGED_IN_KEY',    'E/.=To_8QU/LcXpG:Ir0b;UVTvU/CIZtKQq-) dFMfIYD`/`+y<hhtt$CteiIQpr');
define('NONCE_KEY',        'Ie%P-Rtz=`C9:WIBA_7n0x~3L{Ez|~:]3Q-=-?21N~c|tLIM=&m!S`8fkBC44?q3');
define('AUTH_SALT',        '8!s08f%_#x4CDA~v)e,*-x,j)cBESw(G;|QZixMXHPd;EyGR${HpI gBQ5|95Q^q');
define('SECURE_AUTH_SALT', '!KbX<Z>-l4l)oG.Y<r9koNCnB.8TBZlj4$[|B[&U~JxoNJcJ^zH8d1hH85j>[~+<');
define('LOGGED_IN_SALT',   '+^Ub(S+9OFn$0,ZDAm,DNr/|;:ZH&nP#N!;hvX)kfK[Z?d$G{O?^A5twJ@c,XAq%');
define('NONCE_SALT',       '4>${Gb/*@v+ovk~+EPIDtHuMhF2+0u[0+FgxyGR9^GHv}^|o^1JB3`+-e*x~KaJu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'aa_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
